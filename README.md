![Text banner that says "Stardust" in uppercase, and "Theme 19 by glenthemes" at the bottom](https://64.media.tumblr.com/334c57fd5682c51f02d5591a37f7c15a/25fdc95177496202-25/s1280x1920/ff0e05a89f7937781bd5e4349124dd0f960ef749.png)  
![Screenshot preview of the theme "Stardust" by glenthemes](https://64.media.tumblr.com/7373b49fe26e1b60377149150004273f/25fdc95177496202-2a/s1280x1920/9d8b035b7caa0193dd3cdb54730c86ecc2ac582c.png)

**Theme no.:** 19  
**Theme name:** Stardust  
**Theme type:** Free / Tumblr use  
**Description:** A minimal 1-2 column theme featuring our beloved alienboy Oikawa Tooru from Haikyuu!!  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-07-20](https://64.media.tumblr.com/906837959e439791f792d99f01614213/tumblr_oak949jH2q1ubolzro1_1280.png)  
**Rework date:** 2021-10-06

**Post:** [glenthemes.tumblr.com/post/147642197494](https://glenthemes.tumblr.com/post/147642197494)  
**Preview:** [glenthpvs.tumblr.com/stardust](https://glenthpvs.tumblr.com/stardust)  
**Download:** [pastebin.com/dmsHXd1x](https://pastebin.com/dmsHXd1x)  
**Credits:** [glencredits.tumblr.com/stardust](https://glencredits.tumblr.com/v)
