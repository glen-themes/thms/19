// AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
// yes that's all I have to say.

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;
    
// audio post play button - flaticon.com/free-icon/play-button_152770
var playb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Layer_1' x='0px' y='0px' viewBox='0 0 330 330' style='enable-background:new 0 0 330 330;' xml:space='preserve'> <path id='XMLID_497_' d='M292.95,152.281L52.95,2.28c-4.625-2.891-10.453-3.043-15.222-0.4C32.959,4.524,30,9.547,30,15v300 c0,5.453,2.959,10.476,7.728,13.12c2.266,1.256,4.77,1.88,7.272,1.88c2.763,0,5.522-0.763,7.95-2.28l240-149.999 c4.386-2.741,7.05-7.548,7.05-12.72C300,159.829,297.336,155.022,292.95,152.281z M60,287.936V42.064l196.698,122.937L60,287.936z'/> </svg>";

document.documentElement.style.setProperty('--audioplay','url("' + playb + '")');

// audio post pause button - flaticon.com/free-icon/pause_747384
var pauseb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M154,0H70C42.43,0,20,22.43,20,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C204,22.43,181.57,0,154,0z M164,462c0,5.514-4.486,10-10,10H70c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> <g> <g> <path d='M442,0h-84c-27.57,0-50,22.43-50,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C492,22.43,469.57,0,442,0z M452,462c0,5.514-4.486,10-10,10h-84c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--audiopause','url("' + pauseb + '")');

// audio post 'install audio' button
// feathericons
var cdrii = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg>";

document.documentElement.style.setProperty('--install','url("' + cdrii + '")');

// external link icon
// feathericons
var schtd = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-external-link'><path d='M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6'/><polyline points='15 3 21 3 21 9'/><line x1='10' y1='14' x2='21' y2='3'/></svg>";

document.documentElement.style.setProperty('--ext','url("' + schtd + '")');
    
// 'previous page' svg
// feathericons
var prev = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather'><polyline points='11 17 6 12 11 7'></polyline><polyline points='18 17 13 12 18 7'></polyline></svg>";

document.documentElement.style.setProperty('--BackSVG','url("' + prev + '")');
    
// 'next page' svg
// feathericons
var next = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather'><polyline points='13 17 18 12 13 7'></polyline><polyline points='6 17 11 12 6 7'></polyline></svg>";

document.documentElement.style.setProperty('--NextSVG','url("' + next + '")');

// glen svg
var oqjbx = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='150.000000pt' height='150.000000pt' viewBox='0 0 150.000000 150.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,150.000000) scale(0.100000,-0.100000)' fill='black' stroke='none'> <path d='M635 1320 c-110 -44 -267 -105 -348 -137 -82 -32 -160 -69 -173 -82 l-24 -24 0 -358 c0 -357 0 -358 23 -383 15 -18 103 -58 287 -131 146 -58 272 -105 280 -105 27 0 534 202 554 221 18 17 19 35 17 382 -2 291 0 365 11 373 7 6 36 19 63 29 28 10 75 29 105 42 l55 24 -34 15 c-18 8 -45 14 -59 14 -14 0 -181 -61 -369 -136 l-344 -135 -52 19 c-275 104 -369 145 -353 154 34 19 571 228 586 228 9 0 78 -25 155 -55 147 -58 172 -62 220 -41 l30 14 -35 15 c-122 53 -350 137 -371 137 -13 -1 -114 -36 -224 -80z m-210 -365 c132 -52 249 -95 260 -95 11 0 123 41 248 90 126 50 232 90 238 90 5 0 9 -134 9 -338 l0 -338 -221 -87 c-122 -48 -225 -84 -230 -81 -5 3 -9 81 -9 173 0 147 -2 171 -17 189 -10 10 -66 36 -125 56 l-106 38 -43 -19 -43 -19 121 -43 c67 -24 124 -50 127 -58 3 -8 6 -84 6 -169 0 -117 -3 -154 -12 -154 -7 0 -112 41 -233 89 l-220 89 -3 341 c-1 188 1 341 5 341 5 0 116 -43 248 -95z'/> </g> </svg>";

document.documentElement.style.setProperty('--glenSVG','url("' + oqjbx + '")');

// corner musicplayer headphones icon
// flaticon.com/free-icon/headphones_686591
var muytlls = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='512.000000pt' height='512.000000pt' viewBox='0 0 512.000000 512.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,512.000000) scale(0.100000,-0.100000)' fill='black' stroke='none'> <path d='M2225 5105 c-519 -73 -987 -270 -1376 -576 -176 -139 -229 -224 -228 -362 0 -43 7 -90 18 -117 l17 -45 -67 -85 c-226 -287 -386 -624 -463 -970 -83 -376 -76 -797 20 -1141 l16 -56 -35 -69 c-19 -38 -46 -105 -60 -149 -43 -139 -50 -203 -44 -430 4 -188 7 -220 31 -305 106 -378 393 -658 791 -772 101 -29 474 -39 520 -14 70 38 65 -41 65 1133 l0 1060 -34 39 -34 39 -223 0 c-198 -1 -233 -3 -304 -22 -157 -43 -291 -110 -402 -200 l-52 -43 -10 38 c-35 133 -38 469 -6 677 60 384 207 729 436 1021 47 60 54 65 78 57 14 -4 67 -8 116 -8 114 1 156 20 315 145 255 202 521 329 835 398 266 60 563 60 830 0 319 -70 628 -221 874 -427 119 -100 157 -116 276 -116 55 0 110 4 121 8 18 6 29 -2 71 -55 237 -303 381 -636 438 -1019 32 -209 29 -552 -6 -681 l-9 -37 -73 55 c-86 65 -202 126 -300 160 -117 40 -224 54 -409 54 -183 0 -207 -5 -247 -56 -21 -27 -21 -27 -21 -1086 0 -1167 -4 -1094 60 -1132 38 -22 339 -23 463 -1 432 75 788 433 872 878 10 53 15 141 15 263 0 239 -21 343 -106 525 l-36 76 31 169 c72 399 66 728 -20 1084 -78 320 -236 641 -444 901 l-73 92 20 56 c41 117 0 270 -99 369 -105 105 -315 253 -498 354 -275 151 -592 258 -916 309 -175 28 -582 35 -734 14z m693 -269 c307 -53 614 -159 845 -292 175 -100 410 -274 441 -326 40 -66 -4 -148 -81 -148 -33 0 -52 11 -143 85 -173 141 -319 230 -535 324 -285 125 -559 181 -891 181 -366 0 -655 -66 -979 -225 -191 -93 -281 -150 -429 -269 -103 -83 -124 -96 -157 -96 -90 0 -132 98 -71 167 24 28 122 107 213 175 122 90 374 224 554 293 185 72 323 104 640 149 74 10 504 -3 593 -18z m-1758 -3687 l0 -882 -92 6 c-51 4 -103 9 -115 13 l-23 6 0 859 0 858 33 6 c33 5 92 10 160 13 l37 2 0 -881z m2978 865 l32 -5 0 -858 0 -859 -22 -6 c-13 -4 -62 -9 -110 -13 l-88 -6 0 882 0 883 78 -7 c42 -3 92 -8 110 -11z m-3478 -865 l0 -740 -31 18 c-53 32 -138 113 -184 177 -98 138 -148 286 -160 471 -17 277 52 508 202 675 47 52 151 140 166 140 4 0 7 -333 7 -741z m3896 657 c102 -89 188 -226 235 -376 22 -68 24 -93 24 -275 0 -140 -4 -217 -14 -255 -37 -142 -115 -280 -218 -384 -53 -53 -142 -123 -146 -114 -1 1 -1 337 1 746 l3 743 29 -18 c16 -10 55 -41 86 -67z'/> </g> </svg>";

document.documentElement.style.setProperty('--nota-headphones','url("' + muytlls + '")');

// corner musicplayer 'play'
// flaticon.com/free-icon/play_748134
var uzhszvdj = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 511.999 511.999' style='enable-background:new 0 0 511.999 511.999;' xml:space='preserve'> <g> <g> <path d='M443.86,196.919L141.46,10.514C119.582-2.955,93.131-3.515,70.702,9.016c-22.429,12.529-35.819,35.35-35.819,61.041 v371.112c0,38.846,31.3,70.619,69.77,70.829c0.105,0,0.21,0.001,0.313,0.001c12.022-0.001,24.55-3.769,36.251-10.909 c9.413-5.743,12.388-18.029,6.645-27.441c-5.743-9.414-18.031-12.388-27.441-6.645c-5.473,3.338-10.818,5.065-15.553,5.064 c-14.515-0.079-30.056-12.513-30.056-30.898V70.058c0-11.021,5.744-20.808,15.364-26.183c9.621-5.375,20.966-5.135,30.339,0.636 l302.401,186.405c9.089,5.596,14.29,14.927,14.268,25.601c-0.022,10.673-5.261,19.983-14.4,25.56L204.147,415.945 c-9.404,5.758-12.36,18.049-6.602,27.452c5.757,9.404,18.048,12.36,27.452,6.602l218.611-133.852 c20.931-12.769,33.457-35.029,33.507-59.55C477.165,232.079,464.729,209.767,443.86,196.919z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--blay','url("' + uzhszvdj + '")');

// corner musicplayer 'pause'
// flaticon.com/free-icon/pause_748136
var nfuvx = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M124.5,0h-35c-44.112,0-80,35.888-80,80v352c0,44.112,35.888,80,80,80h35c44.112,0,80-35.888,80-80V80 C204.5,35.888,168.612,0,124.5,0z M164.5,432c0,22.056-17.944,40-40,40h-35c-22.056,0-40-17.944-40-40V80 c0-22.056,17.944-40,40-40h35c22.056,0,40,17.944,40,40V432z'/> </g> </g> <g> <g> <path d='M482.5,352c11.046,0,20-8.954,20-20V80c0-44.112-35.888-80-80-80h-34c-44.112,0-80,35.888-80,80v352 c0,44.112,35.888,80,80,80h34c44.112,0,80-35.888,80-80c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20 c0,22.056-17.944,40-40,40h-34c-22.056,0-40-17.944-40-40V80c0-22.056,17.944-40,40-40h34c22.056,0,40,17.944,40,40v252 C462.5,343.046,471.454,352,482.5,352z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--bause','url("' + nfuvx + '")');

// 'view post' permalink icon
// flaticon.com/free-icon/hyperlink_985368
var gpydkfiya = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='141.000000pt' height='141.000000pt' viewBox='0 0 141.000000 141.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,141.000000) scale(0.100000,-0.100000)' fill='black' stroke='none'> <path d='M924 1356 c-55 -18 -76 -33 -161 -118 -77 -77 -90 -110 -57 -142 28 -29 55 -18 131 55 104 98 159 118 239 84 42 -17 150 -122 164 -160 20 -52 12 -118 -19 -166 -42 -64 -300 -315 -344 -334 -71 -29 -135 -14 -205 49 -46 42 -83 47 -106 14 -24 -33 -20 -44 29 -97 86 -92 218 -120 331 -72 76 33 393 355 420 426 27 71 25 176 -3 235 -30 61 -150 182 -208 210 -57 28 -154 35 -211 16z'/> <path d='M553 960 c-75 -16 -117 -47 -289 -222 -194 -196 -209 -221 -209 -343 0 -52 6 -82 22 -115 29 -59 152 -183 208 -210 63 -30 139 -36 209 -16 49 15 71 30 147 103 94 90 109 122 73 157 -28 29 -55 18 -131 -55 -104 -98 -159 -118 -239 -84 -42 17 -150 122 -164 160 -20 52 -12 118 19 166 42 64 300 315 344 334 71 29 135 14 205 -49 46 -42 83 -47 106 -14 24 33 20 44 -29 97 -70 75 -175 110 -272 91z'/> </g> </svg>";

document.documentElement.style.setProperty('--linksvg','url("' + gpydkfiya + '")');

// 'user' icon in permalink overlay
// source: flaticon.com/free-icon/follower_983929 
var uugv = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='642.000000pt' height='642.000000pt' viewBox='0 0 642.000000 642.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,642.000000) scale(0.100000,-0.100000)' fill='black' stroke='none'> <path d='M2965 6375 c-195 -32 -348 -81 -520 -168 -394 -198 -694 -546 -843 -978 -71 -204 -77 -251 -77 -544 l1 -260 32 -117 c97 -357 279 -660 533 -888 86 -77 114 -107 114 -125 0 -5 -45 -28 -100 -50 -791 -321 -1361 -830 -1720 -1535 -216 -425 -328 -850 -342 -1300 l-5 -185 29 -55 c22 -43 41 -62 83 -87 l54 -33 3006 0 3006 0 54 33 c42 25 61 44 82 86 26 50 28 61 28 172 0 708 -304 1481 -803 2040 -332 373 -782 679 -1274 869 -46 17 -85 36 -89 42 -3 5 56 71 133 146 260 256 413 506 502 825 46 162 54 253 48 492 -6 233 -18 301 -82 484 -203 579 -679 997 -1278 1122 -135 28 -439 35 -572 14z m510 -463 c235 -51 423 -153 601 -324 253 -245 378 -540 377 -893 -1 -470 -255 -892 -663 -1103 -177 -92 -325 -131 -526 -139 -372 -15 -696 114 -950 379 -235 244 -347 524 -347 863 0 357 122 646 377 893 185 179 407 294 647 337 115 20 362 13 484 -13z m-35 -2932 c443 -34 899 -192 1270 -440 221 -148 481 -387 642 -590 224 -282 403 -644 493 -1000 38 -149 66 -325 63 -387 l-3 -58 -285 -6 c-157 -3 -1241 -6 -2410 -6 -1169 0 -2253 3 -2410 6 l-285 6 1 90 c2 156 78 468 167 690 273 678 836 1241 1507 1508 403 160 805 220 1250 187z'/> </g> </svg>";

document.documentElement.style.setProperty('--fol','url("' + uugv + '")');

// star shape
// flaticon.com/free-icon/star_2099156
var zlgrif = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.0' width='128.000000pt' height='128.000000pt' viewBox='0 0 128.000000 128.000000' preserveAspectRatio='xMidYMid meet'> <g transform='translate(0.000000,128.000000) scale(0.100000,-0.100000)' fill='black' stroke='none'> <path d='M598 1238 c-9 -7 -42 -76 -73 -153 -78 -192 -83 -202 -108 -209 -12 -2 -101 -12 -197 -21 -199 -19 -220 -27 -220 -81 0 -39 1 -40 177 -191 55 -49 107 -98 114 -111 11 -19 6 -47 -29 -195 -23 -94 -42 -182 -42 -194 0 -26 35 -53 68 -53 12 0 91 40 175 90 84 49 164 90 177 90 13 0 93 -41 177 -90 84 -50 163 -90 175 -90 34 0 68 27 68 55 0 13 -18 100 -41 194 -23 94 -39 178 -36 185 3 7 65 66 139 131 159 141 158 140 158 182 0 51 -24 59 -220 78 -96 9 -185 19 -196 21 -26 7 -37 27 -114 213 -32 79 -65 148 -72 153 -19 12 -62 10 -80 -4z'/> </g> </svg>";

document.documentElement.style.setProperty('--cutestar','url("' + zlgrif + '")');

// =>

$(document).ready(function(){
    // check jquery version
    var jqver = jQuery.fn.jquery;
    jqver = jqver.replaceAll(".","");
    
    $(".tumblr_preview_marker___").remove();
    
    /*-------- TOOLTIPS --------*/
    $("a[title]:not([title=''])").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:0,
        tip_fade_speed:69,
        attribute:"title"
    });
    
    /*------- WRAP NPF PHOTO CAPTIONS -------*/
    $(".photo-origin").each(function(){
        // remove bottom spacing bc you already have it in css
        $(this).css("margin-bottom","");
        $(this).nextAll().wrapAll("<div class='caption'>")
    })
    
    /*------- IDENTIFY NPF PHOTOSETS W/O CAPTIONS -------*/
    $(".posts[orig-or-rb='original-post']").each(function(){
        var npf1 = $(this).find(".npf_inst").eq(0);
        if(!npf1.prev().length){
            npf1.addClass("photo-origin")
        }
    })
    
    $(".reblog-comment").each(function(){
        if($(this).find(".npf_inst").length){
            var heb = $(this).find(".npf_inst").eq(0);
            
            if($.trim(heb.prev("p").text()) == ""){
                heb.addClass("photo-origin");
                
                if(!$(this).parent().is(".caption")){
                    heb.prev("p").addClass("p-tbd");
                    $(this).prev(".source-head").remove();
                }
            }
        }
    })
    
    /*------- CAPTIONS -------*/
    $(".caption").each(function(){
        // target original post captions (no rb head)
        if(!$(this).find(".reblog-head").length){
            $(this).addClass("op-capt")
        }
        
        // remove empty captions
        if($.trim($(this).text()) == ""){
            $(this).remove();
        }
    })
    
    
    /*---- TARGET PHOTO POSTS W/O CAPTIONS ----*/
    $(".single-photo, .photoset-grid").each(function(){
        if(!$(this).next(".caption").length){
            $(this).parents(".posts").addClass("hov-to-post");
            $(this).parents(".posts").wrapInner("<div class='hov-element'>")
        }
    })
    
    /*----------- NPF VIDEO IDENTIFY -----------*/
    $(".tmblr-full").each(function(){
        if($(this).is("[data-npf*='video'")){
            $(this).parents(".npf_inst").addClass("npf-video");
            
            // if npf video is first reblog
            if(!$(this).parents(".reblog-comment").prev(".reblog-head").length){
                $(this).parents(".npf_inst").addClass("tmblr-video").css("margin-top","calc(0px - var(--Post-Padding))");
            }
        }
    })
    
    /*------- REMOVE NPF VIDEO AUTOPLAY -------*/
    $("video[autoplay='autoplay']").each(function(){
        $(this).removeAttr("autoplay")
    });
    
    /*------- FIX POSTS WITH 'INVALID' TUMBLR URL -------*/
    // owtte "deactivated" but not really, idk why
    $(".caption").each(function(){
        if($(this).find(".reblog-wrap").length == 1){
            var RBhead = $(this).find(".reblog-head").eq(0);
            var opsrcname = $(this).parents(".posts").attr("op-src-name");
            var opsrcurl = $(this).parents(".posts").attr("op-src-url");
            RBhead.find(".reblog-url a").text(opsrcname).attr("href",opsrcurl);
            
            if(RBhead.find("img").attr("src").indexOf("default_avatar") > -1){
                RBhead.find("img").attr("src","//api.tumblr.com/v2/blog/" + opsrcname + ".tumblr.com/avatar/128")
            }
        }
        // go here
    })
    
    /*----------- REMOVE <p> WHITESPACE -----------*/
    $(".postinner p").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().is(".postinner")){

                $(this).css("margin-top",0)
            }
        }
        
        if(!$(this).next().length){
            // target last <p>
            // if it's empty, remove
            if($.trim($(this).html()) == ""){
                $(this).remove();
            }
        }
    })
    
    $(".postinner p, .postinner blockquote, .postinner h1, .postinner h2, .postinner ol, .postinner ul").each(function(){
        if(!$(this).next().length){
            // target last <p>
            // if no next sibling, negate bottom padding
            $(this).css("margin-bottom","1px")
        }
    })
    
    $("pre").each(function(){
        if(!$(this).next().length){
            $(this).css("margin-bottom",0)
        }
    })
    
    /*----------- REBLOG-HEAD -----------*/
    $(".reblog-url").each(function(){
        var uz = $.trim($(this).text());
        if(uz.indexOf("-deac") > 0){
            var rogner = uz.substring(0,uz.lastIndexOf("-"));
            $(this).find("a").attr("href","//" + rogner + ".tumblr.com");
            $(this).find("a").text(rogner);
            $(this).append("<span class='deac'>(deactivated)</span>")
        }
    })
    
    /*-------- AUDIO BULLSH*T --------*/
    var mtn = Date.now();
    var fvckme = setInterval(function(){
        if(Date.now() - mtn > 1000){
            clearInterval(fvckme);
            $(".audiowrap").each(function(){
                $(this).prepend("<audio src='" + $(this).attr("audio-src") + "'>");
            });
            
            $(".inari").each(function(){
                var m_m = $(this).parents(".audiowrap").attr("audio-src");
                $(this).attr("href",m_m);
            })
        } else {
            $(".tumblr_audio_player").each(function(){
                if($(this).is("[src]")){
                    var audsrc = $(this).attr("src");
                    audsrc = audsrc.split("audio_file=").pop();
                    audsrc = decodeURIComponent(audsrc);
                    audsrc = audsrc.split("&")[0];
                    $(this).parents(".audiowrap").attr("audio-src",audsrc)
                }
            })
        }
    },0);
    
    $(".albumwrap").click(function(){
        
        var emp = $(this).parents(".audiowrap").find("audio")[0];
        
        if(emp.paused){
            emp.play();
            $(".overplay",this).addClass("ov-z");
            $(".overpause",this).addClass("ov-y");
        } else {
            emp.pause();
            $(".overplay",this).removeClass("ov-z");
            $(".overpause",this).removeClass("ov-y");
        }
        
        var that = this
        
        emp.onended = function(){
            $(".overplay",that).removeClass("ov-z");
            $(".overpause",that).removeClass("ov-y");
        }
    })
    
    // minimal soundcloud player @ shythemes.tumblr
    var color = getComputedStyle(document.documentElement)
               .getPropertyValue("--Body-Text-Color");
    
    $('iframe[src*="soundcloud.com"]').each(function(){
        $(this).one("load",function(){
            soundfvk()
        });
    });
    
    function soundfvk(){
       $('iframe[src*="soundcloud.com"]').each(function(){
           $(this).attr({ src: $(this).attr('src').split('&')[0] + '&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=false&amp;origin=tumblr&amp;color=' + color.split('#')[1], height: 116, width: '100%' });
       });
    }
    
    $(".soundcloud_audio_player").each(function(){
        $(this).wrap("<div class='audio-soundcloud'>")
    })
    
    /*-------- QUOTE SOURCE BS --------*/
    $(".quote-source").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span>");
        $(this).find("a.tumblr_blog").remove();
        
        $(this).find("span").each(function(){
            if($.trim($(this).text()) == "(via"){
                $(this).remove();
            }
            
            if($.trim($(this).text()) == ")"){
                $(this).remove();
            }
        })
        
        $(this).html(
            $(this).html().replace("(via","")
        )
        
        $(this).find("span:first").each(function(){
            if(!$(this).next().length){
                $(this).remove()
            }
        })
        
        $(this).find("p").each(function(){
            if($.trim($(this).text()) == ""){
                $(this).remove();
            }
            
            if($(this).children("br").length){
                if(!$(this).children().first().siblings().length){
                    $(this).remove()
                }
            }
            
            $(this).html($.trim($(this).html()));
            
            if($(this).text() == ")"){
                $(this).remove()
            }
        })
        
        $(this).find("p:last").each(function(){
            if($(this).find("a[href*='tumblr.com/post']").length){
                $(this).remove()
            }
        })
        
        $(this).find("p:last").each(function(){
            if(!$(this).next().length){
                $(this).css("margin-bottom","3px")
            }
        })
    })
    
    $("[mdash] + p").each(function(){
        if(!$(this).next().length){
            if($.trim($(this).text()) !== ""){
                var sto = " " + $(this).html();
                $(this).prev().append(sto)
                $(this).remove();
            }
        }
    })
    
    /*-------- ASK/ANSWER POSTS --------*/
    $(".question_text").each(function(){
        if(!$(this).children().first().is("p")){
            $(this).wrapInner("<p></p>")
        }
    })
    
    $(".askpot img").each(function(){
        if($(this).attr("src").indexOf("assets.tumblr.com/images/anonymous_avatar_64.gif")){
            $(this).attr("src","//cdn.discordapp.com/attachments/382037367940448256/832538526003494932/tumblr_anon_rm.png")
        }
    })
    
    /*-------- CHAT POSTS --------*/
    $(".npf_chat").each(function(){
        $(this).find("b").each(function(){
            var cb = $(this).html();
            $(this).before("<div class='chat_label'>" + cb + "</div>");
            $(this).remove()
        })
        
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<div class='chat_content'>");
        
        $(this).wrap("<div class='chat_row'>");
        $(this).children().unwrap()
    })
    
    $(".chat_row").each(function(){
        $(this).not(".chat_row + .chat_row").each(function(){
            if(jqver < "180"){
                $(this).nextUntil(":not(.chat_row)").andSelf().wrapAll('<div class="chatwrap">');
            } else {
                $(this).nextUntil(":not(.chat_row)").addBack().wrapAll('<div class="chatwrap">');
            }
        });
    })

    $(".chat_row .chat_content:first").each(function(){
        if($(this).nextAll(".chat_content").length){
            $(this).nextAll().appendTo($(this));
            
        }
    })
    
    $(".chat_row > .chat_content").each(function(){
        var that = this;
        $(this).find(".chat_content").each(function(){
            var xvcy = $(this).html();
            $(this).remove();
            $(that).append(xvcy);
        })
    })
    
    /*---- AUDIO POST EMBEDS (spotify, soundcloud) ----*/
    $(".audio-post").each(function(){
        var that = this;
        if($(this).find("iframe[class*='_audio_player']").length){
            if(!$(this).find(".audiowrap").length){
                $(that).addClass("text-post").removeClass("audio-post").attr("post-type","text")
            }
        }
    })
    
    /*---- MAKE SURE <p> IS FIRST CHILD OF RB ----*/
    $(".reblog-head").each(function(){
        if(!$(this).next(".reblog-comment").length){
            $(this).nextAll().wrapAll("<div class='reblog-comment'>")
        }
    })
    
    $(".reblog-comment").each(function(){
        if($(this).children().first().is("div")){
            $(this).prepend("<p></p>")
        }
    })
    
    /*----------- POST NOTES -----------*/
    $("ol.notes a[title]").each(function(){
        $(this).removeAttr("title")
    });
    
    // remove tumblr redirects script by magnusthemes@tumblr
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });
    
    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();
    
    // make iframe heights look more 'normal'
	$(".embed_iframe").each(function(){
        if($(this).parent().is(".tmblr-embed")){
            var wut = $(this).width();
            
            var wrath = $(this).attr("width");
            var rat_w = wrath / wut;
            
            var hrath = $(this).attr("height");
            var rat_h = hrath / rat_w;
            
            $(this).height(rat_h)
        }
    })
    
    /*--- fvck tvmblr ---*/
    var imgs = document.querySelectorAll("img");
    Array.prototype.forEach.call(imgs, function(invis){	
      if(invis.src.indexOf("assets.tumblr.com/images/x.gif") > -1){
        invis.setAttribute("src","//cdn.glitch.com/bdf00c8f-434a-46d9-a514-ec8332ec176a/1x1.png");
      }
    });
    
    /*----- OTHER -----*/
    if(customize_page){
        $(".reblog-head img").each(function(){
            if($(this).attr("src") == $("html[portrait]").attr("portrait")){
                $(this).remove()
            }
        })
    }
    
    $(".chat_content").each(function(){
        if($.trim($(this).text()).indexOf("{block:") > -1){
            var notgod = $(this).html();
            notgod.replaceAll("{","&lcub;").replaceAll("}","&rcub;");
            $(this).before("<code>" + notgod + "</code>");
            $(this).remove()
        }
    })
    
    /*---------- CORNER MUSIC PLAYER ----------*/
    if($(".mplayer").length){
      var aaa = document.getElementById("musique");

      aaa.volume = 0.69;

      if($("#musique").is("[autoplay]")){
          $(".pausee").addClass("aff");
          $(".playy").addClass("beff");
      }

      $(".music-controls").click(function(){
          if (aaa.paused) {
            aaa.play();
            $(".pausee").toggleClass("aff");
            $(".playy").toggleClass("beff");
          } else {
            aaa.pause();
            $(".playy").toggleClass("beff");
            $(".pausee").toggleClass("aff");
          }
      });

      aaa.onended = function(){
          $(".playy").removeClass("beff");
        $(".pausee").removeClass("aff");
      };

      $("#musique").each(function(){
          var mp3 = $.trim($(this).attr("src"));
          mp3 = mp3.replaceAll("?dl=0","").replaceAll("www.dropbox","dl.dropbox");
          $(this).attr("src",mp3)
      })
    }
    
    $("[indexpage][post-cols='two'] .garlique").masonry({
      // options
      itemSelector:".posts",
      transitionDuration:0
    });
    
    $(".time-ago").timeAgo({
        time: "short", // can be "letter" "short" or "word"
        spaces: true, // adds spaces between words and numbers
        words: false, // turns numbers to words
        prefix: "", // adds a prefix to the outputted string. could be "~" or "about"
        suffix: " ago", // adds a suffix to the outputted string. could be "ago"
    });
    
    $(".time-ago").each(function(){
        var fjs = $.trim($(this).text()).toLowerCase().replaceAll("  "," ");
        if(fjs == "nows ago"){
            $(this).text("just now")
        }
    })
    
    $(".bou-icons").each(function(){
        var ln = $(this).children("a").length;
        
        document.documentElement.style.setProperty('--totalButtonsCount',ln);
        
        $(this).next(".tagsdiv").css("transition","opacity var(--Post-Buttons-Transition-Speed) calc(var(--Post-Buttons-Base-Delay) + (var(--Post-Buttons-Step-Delay) * (" + ln + " - 0))) ease-in-out");
    })
    
    $(".tagsdiv").each(function(){
        var getty = $(this).css("transition-delay");
        $(this).attr("delay",getty);
    })
    
    /*----- CHANGE NPF PHOTO POSTS TO PHOTO CATEGORY -----*/
    $("[post-type='text'] .postinner").each(function(){
        var tfs = $(this).children().first();
        // reblogged npf photosets
        if(tfs.is(".reblog-wrap")){
            if(tfs.children().first().is(".photo-origin")){
                $(this).parents(".posts").attr("post-type","photo");
                $(this).parents(".posts").addClass("photo-post").removeClass("text-post")
            }
        }
        // original npf photosets
        else if(tfs.is(".photo-origin")) {
            $(this).parents(".posts").attr("post-type","photo");
            $(this).parents(".posts").addClass("photo-post").removeClass("text-post");
            tfs.nextAll().wrapAll("<div class='caption'>")
        }
    })
    
    /*---- TARGET SC/SPOTIFY NPF ----*/
    $("figure[data-npf*='soundcloud.com'], figure[data-npf*='spotify.com']").each(function(){
        $(this).addClass("npf-audio");
        
        if($(this).next().length){
            $(this).nextAll().wrapAll("<div class='caption'>")
        }
        
        if($(this).parents(".posts[orig-or-rb='reblog']").find(".npf-audio").length == 1){
            // if npf audio has caption
            if($(this).next().length){
                $(this).parent(".reblog-comment").prev(".source-head").prependTo($(this).next(".caption"));
                
                if($.trim($(this).prev("p").text()) == ""){
                    $(this).prev("p").remove();
                }
            }
            // if npf audio has NO caption
            else {
                if(!$(this).parent(".reblog-comment").parent(".reblog-wrap").next(".reblog-wrap").length){
                    // get rid of empty <p>
                    if($.trim($(this).prev("p").text()) == ""){
                        $(this).prev("p").remove();
                    }
                    
                    // get rid of source head
                    $(this).parent().prev(".source-head").remove();
                }
            }
        }
    })
    
    /*---- TARGET NO-NEED CLICKABLE ELEMENTS ----*/
    $(".caption, .answer-post, .text-post, .quote-post, .link-description, .npf_inst:not(.photo-origin), .audiotxt .odin").each(function(){
        
        $(this).addClass("hov-element");
    })
    
    /*---- IF HIDE CAPTIONS, MAKE ENTIRE POST HOVERABLE ----*/
    $("[indexpage] [capts='hide'] .posts:not(.audio-post)").each(function(){
        $(this).addClass("hov-to-post");
        $(this).wrapInner("<div class='hov-element'>")
    })
    
    /*---- DO THE THING [AGAIN] FOR POSTS WITHOUT ANY CAPTS ----*/
    $(".posts:not(.audio-post)").each(function(){
        if(!$(this).find(".hov-element").length){
            $(this).addClass("hov-to-post");
            $(this).wrapInner("<div class='hov-element'>")
        }
    })
    
    
    
    // the caption, + things that don't need a further click
    $(".hov-element").each(function(){
        if($(this).parents(".posts").length){
            $(this).parents(".posts").addClass("hov-to-post")
        }
        
        if($(this).is("[orig-or-rb]")){
            $(this).addClass("hov-to-post");
            
            if(!$(this).find(".hov-element").length){
                $(this).wrapInner("<div class='hov-element'>")
                $(this).removeClass("hov-element");
            }
        }
    })
    
    // the caption, + things that don't need a further click
    $(".hov-element").hover(function(){
        var that = this;
        var postmama = $(that).parents(".hov-to-post");
        postmama.addClass("hoverlay");
        postmama.find(".tagsdiv").addClass("yee-hov");
    }, function(){
    });
    
    
    $(".l-overlay").hover(function(){
        $(".hoverlay").addClass("hoverlay")
    }, function(){
        $(".hoverlay").removeClass("hoverlay");
        $(".tagsdiv.yee-hov").removeClass("yee-hov")
    })
    
    /*---- REMOVE <p> WHITESPACE PT. 2 ----*/
    $(".caption").each(function(){
        $(this).children().first("p").each(function(){
            if(!$(this).prev().length){
                $(this).css("margin-top",0)
            }
        })
    })
    
    $(".p-tbd").remove();
    
    /*---- REMOVE DEACTIVATED STRING IN HOVER SOURCE ----*/
    $(".sauceco").each(function(){
        var ap = $(this).parent("a");
        var apstr = ap.attr("title").split("@").pop();
        if(apstr.indexOf("-")){
            var aplast = apstr.split("-").pop();
            if(aplast.slice(0,4) == "deac"){
                var apgen = apstr.substring(0,apstr.lastIndexOf("-"));
                var aptitle = ap.attr("title");
                var ap_et = aptitle.substr(0,aptitle.lastIndexOf("-"));
                ap.attr("title",ap_et + " (deactivated)")
            }
        }
    })
    
    /*--- MUCH CHAD VERY SAD ---*/
    $(".navlinks").each(function(){
        if(!$(this).find("a[href*='glenthemes.tumblr.com']").length){
            $("body").eq(0).append("<a class='chadchad' href='//glenthemes.tumblr.com' title='Stardust theme by glenthemes'><span class='notverysmooth'></span></a>");
        }
    })
    
    $(".chadchad").css({
        "display":"block",
        "position":"fixed",
        "bottom":"0",
        "margin-bottom":"15px",
        "right":"0",
        "margin-right":"15px",
        "padding":"var(--MusicPlayer-Circle-Padding)",
        "border-radius":"100%",
        "background":"var(--MusicPlayer-Circle-Background)",
        "box-shadow":"3px 3px 8px 0px rgba(0,0,0,3%)"
    })
    
    $(".notverysmooth").css({
        "display":"block",
        "width":"var(--MusicPlayer-Icon-Size)",
        "height":"var(--MusicPlayer-Icon-Size)",
        "-webkit-mask-size":"contain",
        "-webkit-mask-repeat":"no-repeat",
        "-webkit-mask-position":"center",
        "-webkit-mask-image":"var(--glenSVG)",
        "background":"var(--MusicPlayer-Icon-Color)",
    })

    // fix the gap in npf photosets if there is no rb head
    $(".posts").each(function(){
        var that = this;
        if(!$(this).find(".caption").length){
            $(that).addClass("captionless");
            if(!$(this).find(".reblog-head").length){
                $(this).find(".photo-origin").css("margin-top","calc(0px - var(--Post-Padding)")
            }
        }
    })
    
    // add gap between captionless photo and tags
    // permalink page only
    $(".captionless .permatags").each(function(){
        $(this).css("padding-top","var(--Captions-Gap)")
    })
    
    /*--- MASONRY FALLBACK ---*/
    var hagao = Date.now();
    
    var veve = setInterval(function(){
        if(Date.now() - hagao > 2500){
            clearInterval(veve);
        } else {
            $("[indexpage][post-cols='two'] .garlique").masonry({
              // options
              itemSelector:".posts",
              transitionDuration:0
            });
        }
    },0);
    
	$(window).load(function(){
		$("[indexpage][post-cols='two'] .garlique").masonry({
              // options
              itemSelector:".posts",
              transitionDuration:0
            });
	});
});//end jquery / end ready
