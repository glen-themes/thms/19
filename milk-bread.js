// download button for npf audio

let npfAudio = ".npf-audio-wrapper";

waitForElement(npfAudio, { end: 3000 }).then(() => {
  document.querySelectorAll(npfAudio).forEach(npfAudio => {
    let aud = npfAudio.parentNode.querySelector("audio");
    if(aud){
      let ev = npfAudio.querySelector(".npf-audio-background");
      
      let ea = document.createElement("a");
      ea.classList.add("inari");
      
      let audSrc;
      
      if(aud.matches("[src]")){
        audSrc = aud.getAttribute("src");
      } else if(aud.querySelector("source[src]")){
        audSrc = aud.querySelector("source[src]").getAttribute("src")
      }
      
      if(audSrc){
        ea.href = audSrc;
        ea.target = "_blank"
        ea.title = "download"
      }
      
      ev.appendChild(ea);
      
      let en = document.createElement("span");      
      en.classList.add("int-all");
      
      ea.appendChild(en)
    }
  })
}).catch(err => {
  console.error(err)
})

// fix the negative margins
let postNPFAudio = ".postinner .photo-origin .npf-audio-wrapper";
waitForElement(postNPFAudio, { end: 3000 }).then(() => {
  document.querySelectorAll(postNPFAudio).forEach(postNPFAudio => {
    let po = postNPFAudio.closest(".photo-origin")
    po.classList.add("npf-audio-origin")
  });
}).catch(err => {
  console.error(err)
})
